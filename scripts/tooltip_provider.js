
function tooltipStart(){
	var inputForm = document.getElementById("form");
	var tooltips = inputForm.getElementsByClassName("tooltip");
	var i, j;
	for (i = 0; i < tooltips.length; i++) {
		
		var parentOf = tooltips[i].parentElement;
		var inputs = parentOf.getElementsByTagName("input");
		for (j = 0; j < inputs.length; j++) {
			(function() {
				var tooltip = tooltips[i];
				inputs[j].addEventListener("focus", function() {tooltip.style.visibility = "visible"; } );
				inputs[j].addEventListener("blur", function() {tooltip.style.visibility = "hidden"; } );
			}) ()
		}
	
		tooltips[i].style.visibility = "hidden";
	}
}

window.addEventListener("load", tooltipStart, false);