

function setupVerificationPrompts() {
	var verForm = document.getElementById("form");
	verForm.addEventListener("submit", 
		function(e) { 
			if(!confirm("Czy na pewno chcesz się zapisać do biegu?")) 
				e.preventDefault(); 
		}, false);
	verForm.addEventListener("reset",
		function(e) { 
			if(!confirm("Czy na pewno chcesz usunąć dane?")) 
				e.preventDefault();  
		}, false);
}

window.addEventListener("load", setupVerificationPrompts, false);

