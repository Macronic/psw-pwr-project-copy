var resultSpan;


function getHeight(){
    return window.prompt("Podaj wzrost (w cm, liczba całkowita, np 180)");
}

function getMass(){
    return window.prompt("Podaj masę ciała (w kg, liczba rzeczywista, np 70.5)");
}

function countBmi(height, mass){
    var heightInM = height/100;
    return mass / (heightInM * heightInM);
}

function chooseMessage(bmi){
    if (bmi < 18){
        return "Twoje bmi jest niskie, możesz biegać w butach z lekką amortyzacją";
    }else if(bmi < 26){
        return "Twoje bmi jest w normie, możesz biegać w większości dostępnych butów";
    }else{
        return "Masz wysokie bmi, upewnij się, że wystartujesz w butach z odpowiednio dużą amortyzacją";
    }
}

function setColor(bmi){
    if (bmi < 18){
        return "color:blue";
    }else if(bmi < 26){
        return "color:green";
    }else{
        return  "color:red";
    }
}

function bmiStart(){
    var height = parseInt(getHeight());
    var mass = parseFloat(getMass());
    var bmi = countBmi(height,mass);
    resultSpan.innerHTML = chooseMessage(bmi);
    resultSpan.setAttribute("style",setColor(bmi));
}

function bmiSetListeners(){
    document.getElementById("bmiTrigger").addEventListener("click",bmiStart,false);
    resultSpan = document.getElementById("bmiResult");
}

window.addEventListener("load", bmiSetListeners, false);

