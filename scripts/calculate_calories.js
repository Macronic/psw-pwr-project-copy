var calcCalButton;
var calcCalDiv;
var weightInput;
var bpmInput;
var paceMinInput;
var paceSecInput;
var partialMarkup = "<p>Twój szacowany wydatek energetyczny wynosi: ";
var endingMarkup = "</p>";
var resultList;
var removeButtonId = "removeButton"
var resultHeaderId = "resultHeader";
const distance = 12;

function calcValidateValue(value, name, min, max){
	while (value < min || value > max) {
		value = window.prompt('Wartość która została podana (' + name + ') jest nieprawidłowa! Podaj jeszcze raz:', '0')
		if(value == null)
			return null;
	}
	return value;
}

function calculateCalories(){
	var weight = parseFloat(weightInput.value)
	weight = calcValidateValue(weight, 'waga', 15, 400)
	if (weight === null)
		return;
	
	var paceMin = parseInt(paceMinInput.value)
	paceMin = calcValidateValue(paceMin, 'tempo w minutach', 0, 59)
	if (paceMin === null)
		return;
	
	var paceSec = parseInt(paceSecInput.value)
	paceSec = calcValidateValue(paceSec, 'tempo w sekundach', 0, 59)
	if (paceSec === null)
		return;
	var calories = distance * (paceMin + paceSec / 60.0) / 60.0 * weight * 12;
	window.alert('Twój wydatek kaloryczny w biegu bedzie wynosić: ' + Math.round(calories) + ' kcal');
	appendResult(Math.round(calories), weight, paceMin, paceSec);
}


function calcStart(){
    calcLoadElementsById();
    calcSetListeners();
}

function calcLoadElementsById(){
    calcCalButton = document.getElementById("calcCalButton");
    calcCalDiv = document.getElementById("calcCalDiv");
	weightInput = document.getElementById("weight");
	bpmInput = document.getElementById("bpm");
	paceMinInput = document.getElementById("pacemin");
	paceSecInput = document.getElementById("pacesec");
	resultList = document.getElementById("resultsList");
}

function calcSetListeners(){
    calcCalButton.addEventListener("click", calculateCalories, false);
}

function appendResult(result, weight, paceMin, paceSec){
	if(resultList.children.length === 0){
		createResultHeader();
		createClearButton();
	}
	var listItem = document.createElement('li');
	var elementContent = document.createTextNode(  "Waga: "+ weight+" | Tempo: " + paceMin +
			":"+paceSec+" min/km"	+ " | Spalone kalorie: " + result);
	listItem.appendChild(elementContent);
	resultList.appendChild(listItem);
}

function createResultHeader(){
	var resultHeader = document.createElement('h3');
	var elementContent = document.createTextNode("Lista wyników");
	resultHeader.id = resultHeaderId;
	resultHeader.appendChild(elementContent);
	calcCalDiv.insertBefore(resultHeader,resultList);
}

function createClearButton(){
	var clearButton = document.createElement('button');
	clearButton.innerHTML = "Wyczyść listę";
	clearButton.id = removeButtonId;
	clearButton.addEventListener('click',clearList);
	calcCalDiv.appendChild(clearButton);
}

function clearList(){
	while(resultList.children.length != 0){
		resultList.removeChild(resultList.children.item(0));
	}
	var removeButton = calcCalDiv.children.namedItem(removeButtonId);
	calcCalDiv.removeChild(removeButton);
	var listHeader = calcCalDiv.children.namedItem(resultHeaderId);
	calcCalDiv.removeChild(listHeader);
}

window.addEventListener("load",calcStart,false);
