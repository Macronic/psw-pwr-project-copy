var fontSelect;
var fontOptions = ['Arial', 'Times New Roman', 'Courier New', 'Verdana'];
var text;

function fontStart(){
    fontSelect = document.getElementById("fontSelect");
    populateFontSelectOptions();
    text = document.getElementById("multicolumns");
    fontSelect.addEventListener('input',changeFontStyle);
}

function populateFontSelectOptions(){
    for(var font of fontOptions){
        addFontOption(font);
    }
}

function addFontOption(font){
   var newOption = document.createElement('option');
    newOption.value = font;
    newOption.innerHTML = font;
    fontSelect.appendChild(newOption);
}

function changeFontStyle(){
    var fontName = fontSelect.value;
    text.style ="font-family:'" + fontName +"'";
}

window.addEventListener("load", fontStart, false);