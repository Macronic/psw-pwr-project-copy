var pathPointSize = 40;
var pathPoints = [[486, 175, "Północna Odra", "Malownicza odra tu jest u góry."], [326, 479, "Południowa Odra", "Część przez park na pewno jest miłym fragmentem biegu, wśród przyrody i natury."], [697, 428, "Rozwidlenie", "Tutaj odra jest z dwóch stron! Nie trać tchu, połowa trasy za Tobą!"]];
var currentPointName;
var pointName;
var pointDescription;
var map;

function getPoint(cx, cy) {
	var i;
	for (i = 0; i < pathPoints.length; i++) {
		if (pathPoints[i][0] < cx && pathPoints[i][0] + pathPointSize > cx &&
			pathPoints[i][1] < cy && pathPoints[i][1] + pathPointSize > cy)
			return pathPoints[i];
	}
	return [0, 0, "", ""]
}

function mouseClickOrShiftDescription(e) {
	pointName.innerHTML = getPoint(e.offsetX, e.offsetY)[2];
	pointDescription.innerHTML = getPoint(e.offsetX, e.offsetY)[3];
}

function mouseHoverDescription(e) {
	if (!e.altKey) {
		currentPointName.innerHTML = getPoint(e.offsetX, e.offsetY)[2];
	}
	
	if (e.shiftKey) {
		mouseClickOrShiftDescription(e);
	}
}


function pathStart(){
	currentPointName = document.getElementById("currentPoint");
	pointName = document.getElementById("pointName");
	pointDescription = document.getElementById("pointDescription");
	
	map = document.getElementById("map");
	map.addEventListener("mousemove", mouseHoverDescription);
	map.addEventListener("mousedown", mouseClickOrShiftDescription);
	map.addEventListener("mouseout", mouseClickOrShiftDescription);
}

window.addEventListener("load", pathStart, false);