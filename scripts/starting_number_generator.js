var numberGen;
var numberGeneratedDiv;
var partialMarkup = "<p>Twój proponowany numer startowy to: ";
var endingMarkup = "</p>";
var startingNumber;

function start(){
    loadElementsById();
    setListeners();
}

function loadElementsById(){
    numberGen = document.getElementById("numberGen")
    numberGeneratedDiv = document.getElementById("numberGenerated");
}

function generateNumber(){
    startingNumber =  Math.floor( Math.random() * 2500);
    window.alert("Twój numer startowy to: " + startingNumber);
    showNumberOnPage();
}

function setListeners(){
    numberGen.addEventListener("click",generateNumber,false);
}

function showNumberOnPage(){
    numberGen.disabled = true;
    var oldDiv = numberGeneratedDiv.innerHTML;
    var newDiv = oldDiv + partialMarkup + startingNumber + endingMarkup;
    numberGeneratedDiv.innerHTML = newDiv;
}

window.onload = start;