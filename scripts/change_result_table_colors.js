var tables;

function resultTableStart(){
    tables = [];
    tables.push( document.getElementById("studTable"));
    tables.push( document.getElementById("wrocTable"));
    tables.push( document.getElementById("policeTable"));
    tables.push( document.getElementById("awlTable"));
}
function setTablesColor(headerColor, oddColor,evenColor){
    for(var table of tables){
        var tbody = table.children.item(0);
        tbody.children.item(0).style="background-color:" + headerColor + ";";
        var row;
        for (row = 1; row < tbody.children.length; row++) { 
            if (row % 2 == 1){
                tbody.children.item(row).style="background-color:" + oddColor + ";";
            }
            else{
                tbody.children.item(row).style="background-color:" + evenColor + ";";
            }
        }
    }
}

function setWhiteStyle(){
        setTablesColor('lightgreen','white','lightgray');
}


function setClassicStyle(){
    setTablesColor('aliceblue','wheat','bisque');
}

window.addEventListener("load", resultTableStart, false);